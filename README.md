# EOS project page

### Installation

1. `git clone git@gitlab.com:SUSE-UIUX/eos.git`
2. `cd eos`
3. Make sure you have the corresponding version of Node and NPM:
  a.
  ```
    sudo npm cache clean -f
    sudo npm install -g n
    sudo n 8.4.0
  ```
  b. `npm install -g npm@5.2.0`
3. `npm install --engine-strict`
4. `npm start`
5. visit: http://localhost:3000/
6. Run `npm run browsersync` for livereload.

#### Working on the Backend?

You might want to auto-build everytime changes in the backend are applied:

Install Nodemon: `npm install -g nodemon`
and instead of running `npm start` now simply run `nodemon`

more info: https://github.com/remy/nodemon

### Running lints:

Test all:
`npm run test:all`

Sass:
`npm run test:sass`

JS:
`npm run test:js`

Pug:
`npm run test:pug`

### Need to run more than one node versions for this and other projects?

Consider using one of the following Node version managers:

https://github.com/nodenv/nodenv

https://github.com/tj/n


### Tested for every browser in every device

Thanks to [browserstack](https://www.browserstack.com) and their continous contribution to open source projects, we continously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.