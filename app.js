var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')

/* =============================
    Routes files declarations
============================= */
// index pages
var landingPage = require('./routes/landing-page/index')
var icons = require('./routes/icons/index')
var dashboard = require('./routes/dashboard/index')
var brandColors = require('./routes/colors/index')
var dashboardWriting = require('./routes/writing/index')
// submenu pages
var howTo = require('./routes/colors/how-to')
var eosIconsSet = require('./routes/icons/eos-icons-set')
var iconsWhenToUseThem = require('./routes/icons/when-to-use-them')
var uxWriting = require('./routes/writing/ux-writing')
var brandVoice = require('./routes/writing/brand-voice')
var brandTone = require('./routes/writing/brand-tone')
var conventionsAndRules = require('./routes/writing/conventions-and-rules')
var specialIcons = require('./routes/internal/special-icons')
var animatedIcons = require('./routes/icons/animated-icons')
var iconsUx = require('./routes/icons/icons-ux')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))

/* =============================
       Routes definitions
============================= */
// index pages routes
app.use('/', landingPage)
app.use('/dashboard', dashboard)
app.use('/icons', icons)
app.use('/colors', brandColors)
app.use('/writing', dashboardWriting)
// submenu routes
app.use('/colors/how-to', howTo)
app.use('/icons/when-to-use-them', iconsWhenToUseThem)
app.use('/icons/eos-icons-set', eosIconsSet)
app.use('/icons/animated-icons', animatedIcons)
app.use('/icons/icons-ux', iconsUx)
app.use('/writing/ux-writing', uxWriting)
app.use('/writing/brand-voice', brandVoice)
app.use('/writing/brand-tone', brandTone)
app.use('/writing/conventions-and-rules', conventionsAndRules)
app.use('/internal/special-icons', specialIcons)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
