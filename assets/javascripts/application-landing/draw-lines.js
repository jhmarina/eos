// draw SVG lines as user scrolls
$(document).on('ready', function () {
  initSVGLine('.path-to-consistency')
  initSVGLine('.path-to-market')
})

// Get a reference to the <path>
function initSVGLine (elClass) {
  var path = document.querySelector(elClass)

  // Get length of path
  var pathLength = path.getTotalLength()

  // Make very long dashes (the length of the path itself)
  path.style.strokeDasharray = pathLength + ' ' + pathLength

  // Offset the dashes so the it appears hidden entirely
  path.style.strokeDashoffset = pathLength

  // Jake Archibald says so
  // https://jakearchibald.com/2013/animated-line-drawing-svg/
  path.getBoundingClientRect()

  // When the page scrolls...
  window.addEventListener('scroll', function (e) {
    drawWhenVisible(elClass)
  })

  // check if the element is visible on screen
  function drawWhenVisible (el) {
    var scrolled = $(window).scrollTop()
    var distance = $(el).offset().top
    var minOffset = $(window).height() / 2
    var velocity = 8
    var screenWidth = window.screen.width
    var scrollToVisible = distance - $(window).height() + minOffset < 0 ? 0 : distance - $(window).height() + minOffset
    // if the element is visible, then start drawing it
    if (distance - scrolled <= $(window).height() - minOffset) {
      // What % down is it?
      // https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript/2387222#2387222
      // Had to try three or four differnet methods here. Kind of a cross-browser nightmare.
      // minus the distance to scroll to the visible area scrollToVisible
      // and accelerate the scrolling by 4

      // Check for tablet resolution to increase the line draw acceleration
      if (screenWidth <= 768) {
        velocity = 12
      }

      var scrollPercentage = [(document.documentElement.scrollTop - scrollToVisible + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight)] * velocity

      // Length to offset the dashes
      var drawLength = pathLength * scrollPercentage

      // Draw in reverse
      path.style.strokeDashoffset = pathLength - drawLength

      // When complete, remove the dash array, otherwise shape isn't quite sharp
      // Accounts for fuzzy math
      if (scrollPercentage >= 0.99) {
        path.style.strokeDasharray = 'none'
      } else {
        path.style.strokeDasharray = pathLength + ' ' + pathLength
      }
    } else {
      return false
    }
  }
}
