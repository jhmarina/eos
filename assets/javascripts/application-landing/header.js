// change header after having scrolled
// initial variables
var maxScrolling = '100'
var scrolled, scrolledMax

$(window).bind('scroll', function () {
  getScrolledData()
})

function getScrolledData () {
  scrolled = $(window).scrollTop()

  // detect if the user has scrolled more than the first section (height)
  // to then apply the changes in the header
  if (scrolled >= maxScrolling) {
    headerChanges()
    scrolledMax = true
  }

  if (scrolledMax && scrolled < maxScrolling) {
    headerReset()
    scrolledMax = false
  }
}

function headerChanges () {
  $('.main-menu').addClass('solid-bg')
}

function headerReset () {
  $('.main-menu').removeClass('solid-bg')
}
