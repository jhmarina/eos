$(function () {
  // This check comes from the variable $sd-max from assets/stylesheets/base/variables/media-query.scss
  // If at some point we change the value of $sd-max don't forget to chage this part aswell.
  if (window.innerWidth < 769) {
    $('.mobile-submenu-landing').slideUp()

    // Toggle nav on click
    $('.landing-mobile-burger').click(function () {
      if (!$('.main-menu').hasClass('solid-bg')) {
        $('.main-menu').toggleClass('solid-bg')
      }

      $('.mobile-submenu-landing').slideToggle(100)
    })
  }
})
