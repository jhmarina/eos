$(function () {
  // Append element to page body.
  $('body').append("<button class='back-to-top'><i class='material-icons md-36'>keyboard_arrow_up</i></button>")
  $('.back-to-top').css('visibility', 'hidden')
  $('.back-to-top').on('click', function () {
    $('html, body').animate({scrollTop: 0}, 500)
  })
})

$(document).scroll(function () {
  var position = $(this).scrollTop()
  if (position > 800) {
    $('.back-to-top').stop().fadeIn().css('visibility', 'visible')
  } else {
    $('.back-to-top').stop().fadeOut()
  }
})
