$(function () {
  // Select the mobile menu elements
  var mobileMenu = $('.mobile-menu').find('nav')
  var submenuList = $('.js-submenu')

  // Hide Nav and submenus
  mobileMenu.slideUp()
  submenuList.slideUp()

  // Toggle mobile menu on click
  $('.burger-menu').click(function () {
    mobileMenu.slideToggle(100, function () {
      // find active primary menu items and open their submenu
      $('a.active').siblings(submenuList).slideDown()
      // find active submenu items and open their submenu
      $('a.active').closest(submenuList).slideDown()
    })
  })

  // Slide down the submenus
  $('.js-open-submenu').click(function () {
    $(this).next(submenuList).slideToggle(400)
  })
})
