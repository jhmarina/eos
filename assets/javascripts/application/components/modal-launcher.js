$(function () {
  // when a video modal closes
  $('.video-modal').on('hidden.bs.modal', function () {
    // Find its video and pause it
    var $video
    $video = $(this).find('video')[0]
    $video.pause()
  })
})
