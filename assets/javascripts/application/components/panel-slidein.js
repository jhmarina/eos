$(document).ready(function () {
  $('.js-panel-slidein-open').on('click', panelSlideinVisible)
  function panelSlideinVisible () {
    $('.panel-slidein').addClass('visible')
  }
  $('.panel-slidein-close').on('click', panelSlideinClose)
  function panelSlideinClose () {
    $('.panel-slidein').removeClass('visible')
  }
})
