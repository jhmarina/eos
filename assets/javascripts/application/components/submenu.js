$(function () {
  // global vars
  var menuLimit = 7
  var submenuLinks = $('.js-submenu-visible a')
  var submenuMore = $('.js-submenu-more')

  // get each nav element's width and set it as attribute to be used later
  submenuLinks.each(function (index) {
    $(this).attr('data-width', $(this).outerWidth())
  })

  menuCollapse()

  // when window is resized, run our collapse function
  $(window).resize(menuCollapse)

  function menuCollapse () {
    var navWidth = 0
    var submenuMoreContent = $('.js-submenu-more-list')
    var submenuWidth = $('.js-submenu-section').width()

    // if the amount of nav items is greater than 7, display the dropdown
    if (submenuLinks.length > menuLimit) {
      submenuMore.show()
    } else {
      submenuMore.hide()
    }

    // cleanup dom when resizing
    submenuMoreContent.html('')

    // 'smart' responsiveness
    submenuLinks.each(function (index) {
      // use the width data attr we setup before
      navWidth += $(this).data('width')

      // possible combination of elements
      var moreSearchWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth()
      var moreUserWidth = $('.js-submenu-more').outerWidth() + $('.user-submenu').outerWidth()
      var combinedWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth() + $('.user-submenu').outerWidth()

      // cache conditions for readability
      var condLimit = index > menuLimit - 1
      // default condition (no search or user)
      var condDefault = navWidth > submenuWidth - 70
      // more dropdown & search
      var condMoreSearch = navWidth > submenuWidth - moreSearchWidth
      // more dropdown & user
      var condMoreUser = navWidth > submenuWidth - moreUserWidth
      // all dropdowns
      var condCombined = navWidth > submenuWidth - combinedWidth

      // if any of these conditions are met
      if (condLimit || condDefault || condMoreSearch || condMoreUser || condCombined) {
        // hide the nav item
        $(this).addClass('hide')
        // clone it, strip it from any class and move it to the dropdown
        $(this).clone().removeClass().appendTo(submenuMoreContent)
        // show the dropdown
        submenuMore.show()
      } else {
        // if none of these conditions are met, ensure that we show the nav items
        $(this).removeClass('hide')
      }
    })
  }
})
