$(function () {
  $('.js-animated-icons .js-panel-slidein-open').on('click', function () {
    var iconAnimatedSelected = $(this).children('.icon-animated').data('icon')
    console.log(iconAnimatedSelected)
    toggleAnimatedIconInPanel(iconAnimatedSelected)
  })
})

function toggleAnimatedIconInPanel (iconAnimatedSelected) {
  $('.js-animated-icons-name').text(iconAnimatedSelected)
  // get information for icon
  getInfoFromAnimatedIconService(iconAnimatedSelected, function (data) { // eslint-disable-line no-undef
    var notFoundMsg = 'No information available'
    var iconTitle = data ? data.title || notFoundMsg : notFoundMsg
    var iconExample = data ? data.example || notFoundMsg : notFoundMsg
    var iconCss = data ? data.cssLink || notFoundMsg : notFoundMsg
    var textDo = data ? data.do || notFoundMsg : notFoundMsg
    var textDont = data ? data.dont || notFoundMsg : notFoundMsg
    var textAction = data ? data.action || notFoundMsg : notFoundMsg
    var tags = data ? data.tags || notFoundMsg : notFoundMsg
    addAnimatedIconSpecificData(iconTitle, iconExample, iconCss, textDo, textDont, tags, textAction)
  })
}

function addAnimatedIconSpecificData (iconTitle, iconExample, iconCss, textDo, textDont, tags, textAction) {
  $('.js-animated-icons-name').html(jQuery.parseHTML(iconTitle))
  $('.js-animated-icons-html').html(jQuery.parseHTML(iconExample))
  $('.js-animated-icons-html-example').text(iconExample)
  $('.js-animated-icons-css-link').prop('href', iconCss)
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))
  $('.js-eos-icons-action').html(jQuery.parseHTML(textAction))
  $('.js-eos-icons-tags').text(tags)
}
