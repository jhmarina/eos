$(document).ready(function () {
  var currentPath = window.location.pathname
  var eosIconsPath = '/icons/eos-icons-set'
  var isInEosIconsPath = currentPath.indexOf(eosIconsPath) !== -1

  if (isInEosIconsPath) {
    getIconsCollectionService(function (dataGlyph, dataPackage) { // eslint-disable-line no-undef
      // data manipulation from glyph-list.json
      var dataGlyphCollection = dataGlyph[0]
      var repositoryUrl = dataGlyphCollection.repositoryUrl
      linkRepositoryUrl(repositoryUrl)
      var baseClass = dataGlyphCollection.baseClass
      addClassToCode(baseClass)
      var iconsGlyphs = dataGlyphCollection.glyphs
      renderIcons(iconsGlyphs)

      // data manipulation from package.json
      var dataPackageCollection = dataPackage[0]
      var version = dataPackageCollection.version
      addVersionNumber(version)
    })
  }

  $('.js-eos-icons-set .js-panel-slidein-open').on('click', function () {
    var iconSelected = $(this).children('.eos-icons').text()
    toggleIconInPanel(iconSelected)
  })
})

function renderIcons (collection) {
  var $iconsContainer = $('.js-eos-icons-list')
  var iconDisplayTemplate = $('.icon-display').clone(true)
  $('.icon-display').remove()
  for (var i = 0; i < collection.length; i++) {
    var newIconDisplay = iconDisplayTemplate.clone(true)
    var iconName = collection[i]
    // add icon name
    $(newIconDisplay).find('.eos-icons').text(iconName)
    $(newIconDisplay).find('.icon-name').text(iconName)
    $($iconsContainer).append(newIconDisplay)
  }
}

function linkRepositoryUrl (url) {
  $('.js-eos-icons-repo-link').attr('href', url)
}

function addVersionNumber (v) {
  $('.js-eos-icons-version').text(v)
}

function addClassToCode (baseClass) {
  $('.js-eos-icons-base-class').text(baseClass)
}

function toggleIconInPanel (iconName) {
  $('.js-eos-icons-name').text(iconName)
  // get information for the Do and Dont and the Tags
  getMoreInfoFromIconService(iconName, function (data) { // eslint-disable-line no-undef
    var notFoundMsg = 'No information available'
    var textDo = data ? data.do || notFoundMsg : notFoundMsg
    var textDont = data ? data.dont || notFoundMsg : notFoundMsg
    var tags = data ? data.tags || notFoundMsg : notFoundMsg
    addIconSpecificData(textDo, textDont, tags)
  })
}

function addIconSpecificData (textDo, textDont, tags) {
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))
  $('.js-eos-icons-tags').text(tags)
}
