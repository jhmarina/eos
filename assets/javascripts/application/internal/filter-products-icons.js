$(document).on('ready', function () {
  // when clicking on one of the products labels in the filter
  $('.js-filter-product-icons').on('click', function () {
    var productName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterProductsOut(productName)
  })
  countIconsProducts()
})

function filterProductsOut (product) {
  var $iconsContainer = $('.icons-list article')
  for (var i = 0; i < $iconsContainer.length; i++) {
    var productLabel = $($iconsContainer[i]).find('.js-product-label').text()
    if (productLabel === product || product === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

function countIconsProducts () {
  var $allProducts = $('.js-count-product-icons')
  $.each($allProducts, function (i, v) {
    var productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    var $counterContainer = $(v)
    var $iconsContainer = $('.icons-list article .js-product-label:contains(' + productName + ')')
    var countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}
