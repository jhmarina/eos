$(function () {
  // Set pathnames for internal pages.
  var internalPages = ['/internal/special-icons', '/icons/when-to-use-them']

  // Location and storage variables delcaration
  var getLocation = window.location
  var selectLocalStorage = window.localStorage

  // Redirect not-allowed users to /dashboard
  function redirectUrl () {
    for (var i = 0; i < internalPages.length; i++) {
      if (getLocation.pathname === internalPages[i]) {
        // Hide the HTML before the redirect.
        $('html').css('display', 'none')
        window.location.replace('/dashboard')
      }
    }
  }

  // Internal members should paste this line into console to set
  // the localStorage variable checkLocal to getAccess
  // localStorage.setItem('checkLocal', 'getAccess');

  // Check if localStorage have checkLocal with value of getAccess
  if (selectLocalStorage.getItem('checkLocal') === 'getAccess') {
    $('.internal-use').css('display', 'inline-block')
  } else {
    redirectUrl()
  }
})
