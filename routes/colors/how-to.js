var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('colors/how-to', { title: 'Colors - How to use', path: req.originalUrl })
})

module.exports = router
