var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('colors/index', { title: 'Brand colors', path: req.originalUrl })
})

module.exports = router
