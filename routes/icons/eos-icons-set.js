var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('icons/eos-icons-set', { title: 'EOS icons set', path: req.originalUrl })
})

module.exports = router
