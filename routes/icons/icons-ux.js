var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('icons/icons-ux', { title: 'EOS icons UX', path: req.originalUrl })
})

module.exports = router
