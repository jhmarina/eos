var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('icons/index', { title: 'EOS icons', path: req.originalUrl })
})

module.exports = router
