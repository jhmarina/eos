var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('icons/when-to-use-them', { title: 'Icons: Do and dont', path: req.originalUrl })
})

module.exports = router
