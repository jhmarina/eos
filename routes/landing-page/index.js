var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('landing-page/index', { title: 'EOS Design System', path: req.originalUrl })
})

module.exports = router
