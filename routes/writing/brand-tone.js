var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/brand-tone', { title: 'Brand Tone', path: req.originalUrl })
})

module.exports = router
