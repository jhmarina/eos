var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/brand-voice', { title: 'Brand Voice', path: req.originalUrl })
})

module.exports = router
