var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/conventions-and-rules', { title: 'Conventions and rules', path: req.originalUrl })
})

module.exports = router
