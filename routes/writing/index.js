var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/index', { title: 'Writing Guides', path: req.originalUrl })
})

module.exports = router
